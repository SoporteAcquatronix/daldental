<!DOCTYPE html>
<html lang="en">


<head>
	<?php require $_SERVER['DOCUMENT_ROOT'].'/assets/components/head.html'; ?>
	<title>Daldental - Consultorio Dental</title>
	<meta name="description" content="">
	<style>
		@media (min-width: 992px) {
			.myh1{
				font-size:5.5rem;
			}
		}
	</style>
</head>

<body>

	<!-- Navigation -->
	<?php require $_SERVER['DOCUMENT_ROOT'].'/assets/components/nav.html'; ?>
	<br>
	<br>
	<header>
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner " role="listbox">
				<div class="carousel-item active" style="background-image: url('/assets/images/slide3.png')">
					<div class="carousel-caption d-none d-md-block">

					</div>
				</div>
				<!-- Slide One - Set the background image for this slide in the line below -->
				<div class="carousel-item " style="background-image: url('/assets/images/slide1.jpg')">
					<div class="carousel-caption  d-md-block">
						<h1 class="text-dark myh1" >Los mejores especialistas</h1>
						<p class="lead">Conoce nuestras especialidaes...</p>
						<a class="nav-link js-scroll-trigger" href="#especialidades"><button class="btn bg-primary text-light">Conocer más</button></a>
					</div>
				</div>
				<!-- Slide Two - Set the background image for this slide in the line below -->
				<div class="carousel-item" style="background-image: url('/assets/images/slide2.jpg')">
					<div class="carousel-caption d-md-block">
						<h1 class="text-dark myh1">Agenda tu cita</h1>
						<p class="lead text-dark">Envianos un correo para agendar...</p>
						<a class="nav-link js-scroll-trigger" href="#Agenda"><button class="btn bg-primary text-light">Agendar</button></a>
					</div>
				</div>
				<!-- Slide Three - Set the background image for this slide in the line below -->

			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</header>

	<section class="content-section" id="especialidades">
		<div class="container">
			<div class="content-section-heading text-center">
				<h3 class="text-secondary mb-0"></h3>
				<h2 class="mb-5 text-dark">Especialidades</h2>
			</div>
			<div class="row no-gutters">
				<div class="col-lg-6">
					<a class="portfolio-item" href="/Ortodoncia/">
						<span class="caption">
							<span class="caption-content">
								<h1>Ortodoncia</h1>
								<p class="mb-0">Conocer más...</p>
							</span>
						</span>
						<img class="img-fluid" src="/assets/images/ortodoncia.jpg" alt="Ortodoncia">
					</a>
				</div>
				<div class="col-lg-6">
					<a class="portfolio-item" href="/Endodoncia/">
						<span class="caption">
							<span class="caption-content">
								<h1>Endodoncia</h1>
								<p class="mb-0">Conocer más...</p>
							</span>
						</span>
						<img class="img-fluid" src="/assets/images/endodoncia.jpg" alt="Endodoncia">
					</a>
				</div>
				<div class="col-lg-4">
					<a class="portfolio-item" href="/Cirugia_maxilofacial/">
						<span class="caption">
							<span class="caption-content">
								<h2>Cirugia Maxilofacial</h2>
								<p class="mb-0">Conocer más...</p>
							</span>
						</span>
						<img class="img-fluid" src="/assets/images/cirugia-maxilofacial-cabecera.jpg" alt="Maxilofacial">
					</a>
				</div>
				<div class="col-lg-4">
					<a class="portfolio-item" href="/Patologia/">
						<span class="caption">
							<span class="caption-content">
								<h1>Patología</h1>
								<p class="mb-0">Conocer más...</p>
							</span>
						</span>
						<img class="img-fluid" src="/assets/images/patologia.jpg" alt="Patología">
					</a>
				</div>
				<div class="col-lg-4">
					<a class="portfolio-item" href="Odontopediatria/">
						<span class="caption">
							<span class="caption-content">
								<h2>Odontopediatria</h2>
								<p class="mb-0">Conocer más...</p>
							</span>
						</span>
						<img class="img-fluid" src="/assets/images/odontopediatria.jpg" alt="Odontopediatria">
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="content-section bg-primary text-white" id="Agenda">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading text-uppercase">Agenda tu cita</h2>
					<h3 class="section-subheading text-muted">Escribenos y agenda tu cita con nosotros.</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<form onsubmit="enviar();">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control" id="name" type="text" placeholder="Nombre *" required="required" data-validation-required-message="Please enter your name.">
									<p class="help-block text-danger"></p>
								</div>
								<div class="form-group">
									<input class="form-control" id="email" type="email" placeholder="Email *" required="required" data-validation-required-message="Please enter your email address.">
									<p class="help-block text-danger"></p>
								</div>
								<div class="form-group">
									<input class="form-control" id="phone" type="number" placeholder="Télefono *" required="required" data-validation-required-message="Please enter your phone number.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control" id="fecha" type="date" placeholder="Fecha *" required="required" data-validation-required-message="Please enter your phone number.">
									<p class="help-block text-danger"></p>
								</div>
								<div class="form-group">
									<textarea class="form-control" id="message" placeholder="Mensaje *" required="required" data-validation-required-message="Please enter a message."></textarea>
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-12 text-center">
								<div id="success"></div>
								<button id="sendMessageButton" class="btn btn-success btn-xl text-uppercase" type="submit">Enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading text-uppercase">Visitanos</h2>
					<h3 class="section-subheading text-muted">Tuxpan 10, Roma Sur, 06760 Ciudad de México.</h3>
				</div>
			</div>
		</div>
	</section>
	<!-- Page Content -->
	<section id="contact" class="map">
		<iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Tuxpan+10,+Roma+Sur,+06760+Ciudad+de+México;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
		<br>
		<small>
			<a rel="nofollow" href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Tuxpan+10,+Roma+Sur,+06760+Ciudad+de+México,+CDMX;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>
		</small>
	</section>

	<!-- Footer -->
	<?php require $_SERVER['DOCUMENT_ROOT'].'/assets/components/footer.html'; ?>
</body>

</html>

<!-- Navigation -->
