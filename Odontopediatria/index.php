<!DOCTYPE html>
<html lang="en">

<head>
	<?php require $_SERVER['DOCUMENT_ROOT'].'/assets/components/head.html'; ?>
	<title>Daldental - Odontopediatria</title>
	<meta name="description" content="">
</head>

<body>
	<?php require $_SERVER['DOCUMENT_ROOT'].'/assets/components/nav.html'; ?>
	<br>
	<br>
	<br>
	<br>
	<!-- Page Content -->
	<div class="container">
		<!-- Portfolio Item Row -->
		<div class="row">

			<div class="col-md-8">
				<img class="img-fluid" src="/assets/images/odontopediatria.jpg" alt="odontopediatria">
			</div>

			<div class="col-md-4">
				<h1 class="my-3 text-primary">Odontopediatria</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
				<h3 class="my-3">Project Details</h3>
				<ul>
					<li>Lorem Ipsum</li>
					<li>Dolor Sit Amet</li>
					<li>Consectetur</li>
					<li>Adipiscing Elit</li>
				</ul><br>
				<center><button type="button" class="btn btn-success btn-lg text-secondary" data-toggle="modal" data-target="#contactoModal">Agendar Cita</button></center>
			</div>

		</div>
		<!-- /.row -->

		<!-- Related Projects Row -->
		<h3 class="my-4 text-primary">Otras Especialidades</h3>

		<div class="row">
			<div class="col-md-3 col-sm-6 mb-4">
				<a href="/Ortodoncia/">
					<img class="img-fluid" src="/assets/images/ortodoncia.jpg" alt="ortodoncia">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 mb-4">
				<a href="/Endodoncia/">
					<img class="img-fluid" src="/assets/images/endodoncia.jpg" alt="endodoncia">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 mb-4">
				<a href="/Cirugia_maxilofacial/">
					<img class="img-fluid" src="/assets/images/cirugia-maxilofacial-cabecera.jpg" alt="max">
				</a>
			</div>

			<div class="col-md-3 col-sm-6 mb-4">
				<a href="/Patologia/">
					<img class="img-fluid" src="/assets/images/patologia.jpg" alt="patologia">
				</a>
			</div>

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

	<!-- Footer -->
	<?php require $_SERVER['DOCUMENT_ROOT'].'/assets/components/footer.html'; ?>
	<?php require $_SERVER['DOCUMENT_ROOT'].'/assets/components/contactomodal.html'; ?>
</body>

</html>

<!-- Navigation -->
